/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author usrsig15
 */
public class CargadorArchivos {

    private File archivocargado = null;
   
    
    public File CargaArchivo(String fileName, InputStream in, String path) {
        System.out.println("Copying file in server....");
        try {


           
            archivocargado = new File(path + File.separator + fileName);
            OutputStream out = new FileOutputStream(archivocargado);

            int read = 0;
            byte[] bytes = new byte[5024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();


        } catch (IOException e) {
            System.out.println("Error copiando archivo en el objeto CargadorArchivos de la libreria SIAMLibrary metodo CargaArchivo: " + e.toString());
        }
        return archivocargado;
    }
}

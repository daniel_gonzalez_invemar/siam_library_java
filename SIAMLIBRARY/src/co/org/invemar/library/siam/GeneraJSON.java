/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class GeneraJSON {

    public StringBuffer getJSON(Connection con, String datasource,String tq) {

        StringBuffer JSON = new StringBuffer();

        PreparedStatement pstmt = null;
        ResultSet rs = null;        
        String query = "select * from table(indicadores.gdatasource.get_json('" + datasource + "','"+tq+"'))";
        try { 
            pstmt = con.prepareStatement(query);

            rs = pstmt.executeQuery();
            while (rs.next()) {               
                JSON.append(rs.getString("COLUMN_VALUE"));
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(GeneraJSON.class.getName()).log(Level.SEVERE, ex.getMessage(), ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GeneraJSON.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return JSON;
    }
    
  

    public static void main(String[] args) {
        GeneraJSON json = new GeneraJSON();
        ConexionPrueba cp = new ConexionPrueba();
        System.out.println("json:"+json.getJSON(cp.getConn(),"climares-listaestacionvar"," select *  WHERE ID_ESTACION LIKE 38885").toString());

       
    }

}

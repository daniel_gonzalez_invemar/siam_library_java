package co.org.invemar.library.siam;

import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase de conexion a la base dedatos autor: Rafael E. Lastra C.<br> Version:
 * 1.0 <br> Fecha: 21-03-2001
 */
public class ConexionPrueba {

    private Connection aCon;
    public String Error;

    public ConexionPrueba(String user, String password) {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@192.168.3.77:1521:sci";
            aCon = DriverManager.getConnection(url, user, password);
            
        } catch (Exception e) {
            Error = e.getMessage();
        }
    }

    public ConexionPrueba() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@192.168.3.77:1521:sci";
            aCon = DriverManager.getConnection(url, "monitoreom", "mon2007");
            
        } catch (Exception e) {
            Error = e.getMessage();
        }
    }

    public Connection getConn() {
        return aCon;
    }

    public void close() {
        try {
            aCon.close();
        } catch (Exception e) {
            Error = e.getMessage();
        }
    }
}

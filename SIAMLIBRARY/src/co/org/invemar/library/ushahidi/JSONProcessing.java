/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.library.ushahidi;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JSONProcessing {
     String pathJsonServices;
     
    public JSONProcessing( String pathJsonServices){
        this.pathJsonServices = pathJsonServices;
    }


    public ArrayList<Incident> JSONToObject() {
        //String urlString = "http://cinto.invemar.org.co/egreta/api?task=incidents";
        BufferedReader reader = null;
        
        ArrayList<Incident> Incidents = new ArrayList<Incident>();
        
        String JSONString;
        try {
            URL url = new URL(pathJsonServices);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[5024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            JSONString = buffer.toString();

            JSONTokener tokener = new JSONTokener(JSONString);
            JSONObject root = new JSONObject(tokener);

            // System.out.println("domain:"+root.getJSONObject("payload").getString("domain"));

            JSONArray jsonArray = root.getJSONObject("payload").getJSONArray("incidents");


            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i).getJSONObject("incident");
                JSONArray jsonCategories = jsonArray.getJSONObject(i).getJSONArray("categories");
                JSONObject jsonCustomFields = jsonArray.getJSONObject(i).getJSONObject("customfields");

                Incident incident = new Incident();


                incident.setId(jsonObject.getString("incidentid"));
                incident.setTitle(jsonObject.getString("incidenttitle"));
                incident.setDescripcion(jsonObject.getString("incidentdescription"));
                incident.setDate(jsonObject.getString("incidentdate"));
                incident.setActive(jsonObject.getString("incidentactive"));
                incident.setLocationName(jsonObject.getString("locationname"));
                incident.setLatitute(jsonObject.getString("locationlatitude"));
                incident.setLongitude(jsonObject.getString("locationlongitude"));

               // System.out.println("Incident id:" + incident.getId());


                ArrayList<Category> categories = new ArrayList<Category>();

                for (int j = 0; j < jsonCategories.length(); j++) {
                    JSONObject jsonCateogy = jsonCategories.getJSONObject(j).getJSONObject("category");
                    Category category = new Category();
                    category.setId(jsonCateogy.getInt("id"));
                    category.setTitle(jsonCateogy.getString("title"));
                    categories.add(category);
                    //System.out.println("--Category id:" + category.getId());
                }
                incident.setCategory(categories);


                ArrayList<CustomFields> customFieldsList = new ArrayList<CustomFields>();
                
                JSONArray jaa = jsonCustomFields.names();      
                           
               
                for (int z = 0; z < jaa.length(); z++) {                    
                        int numero = jaa.getInt(z);           
                    
                        JSONObject jsonCustomsField =  jsonCustomFields.getJSONObject(String.valueOf(numero));                      

                        CustomFields customFields = new CustomFields();
                        customFields.setFieldId(jsonCustomsField.getString("field_id"));
                        customFields.setFormdId(jsonCustomsField.getString("form_id"));
                        customFields.setFieldName(jsonCustomsField.getString("field_name"));
                        customFields.setFieldType(jsonCustomsField.getString("field_type"));
                        customFields.setFieldDefault(jsonCustomsField.getString("field_default"));
                        customFields.setFieldResponse(jsonCustomsField.getString("field_response"));
                        customFieldsList.add(customFields);
                    
                       // System.out.println("----Custom field name:" + customFields.getFieldName());
                    
                    }
                incident.setCustomsFieldList(customFieldsList);
                Incidents.add(incident);
            }           


        } catch (MalformedURLException e) {
            System.out.println("error:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("error:" + e.getMessage());
        } catch (JSONException e) {
            System.out.println("error:" + e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException f) {
                }
            }
        }
        return Incidents;
    }
    public static void main(String[] args) {
        
        JSONProcessing jsonproccesing = new JSONProcessing("http://cinto.invemar.org.co/egreta/api?task=incidents");
        ArrayList<Incident> incidentList=jsonproccesing.JSONToObject();
        System.out.println("incident list:"+incidentList.size());
        
        
    }

    
}


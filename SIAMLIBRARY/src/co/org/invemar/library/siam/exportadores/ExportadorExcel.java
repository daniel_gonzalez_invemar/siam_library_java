/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam.exportadores;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import co.org.invemar.library.siam.exportadores.Data;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author usrsig15
 */
public class ExportadorExcel {

    private String nombreArhivo;
    private String pathArchivo;
    private ArrayList arrayDatos = null;
    private ArrayList<Rows> arrayRows = null;
    private ArrayList<String> arrayData =null;
    private String columnaDatos = null;
    private HSSFWorkbook workbook = null;
    private int cantidadColumnas = 0;
    HSSFSheet firstSheet = null;
    private String metadataFirtSheet = null;
    private byte[] filebyte = null;
    private static final int SECOND_ROW_SHEET = 1;
    private static final int FIRST_ROW_SHEET=0;
    private String columnSeparator="";
    private String dataSeparator="";
    

    public ExportadorExcel(String sheetName, ArrayList arrayDatos, String columnaDatos, String pathArchivo) {
        this.columnaDatos = columnaDatos;
        this.arrayDatos = arrayDatos;
        this.pathArchivo = pathArchivo;
        nombreArhivo = sheetName;

    }

    public ExportadorExcel(String sheetName, String columnDatos, ArrayList<Rows> data, String pathArchivo) {
        this.nombreArhivo = sheetName;
        this.columnaDatos = columnDatos;
        this.arrayRows = data;
        this.pathArchivo = pathArchivo;
    }
    
    public ExportadorExcel(String pathArchivo,String sheetName,String columnaDatos,ArrayList<String> arrayData,String columnSeparator, String dataSeparator ) {
        this.nombreArhivo = sheetName;     
        this.pathArchivo = pathArchivo;
        this.columnaDatos= columnaDatos;
        this.columnSeparator=columnSeparator;
        this.dataSeparator=dataSeparator;
        this.arrayData= arrayData;
    }
    
    
     private void setHeader(Sheet sheet){
            Row rowHeader = sheet.createRow((short) 0);

            String[] nombreColumnas = columnaDatos.split(columnSeparator);
            int contadorCeldas = 0;
            for (int j = 0; j < nombreColumnas.length; j++) {
                String nombreColumna = nombreColumnas[j];
                Cell cell = rowHeader.createCell(contadorCeldas);
                cell.setCellValue(nombreColumnas[j]);
                contadorCeldas++;
            }
    }
    private void fillRows(Sheet sheet){
            
           
            int rowAcount = SECOND_ROW_SHEET;            
            Iterator<String> iteratorArrayData = arrayData.iterator();
            
           
            while (iteratorArrayData.hasNext()) {   
                
              
                
                Row row = sheet.createRow(rowAcount);
                String data =iteratorArrayData.next();
                
                String[] arrayData = data.split(dataSeparator); 
                for (int i = 0; i < arrayData.length; i++) {
                    Cell cell = row.createCell(i);                   
                    cell.setCellValue(arrayData[i]);    
                }               
               rowAcount++;
            }
    }
    
    public byte[] fillFirstSheet() {
       
        byte[] archivo = null;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XSSFWorkbook libro = null;
        try {

            libro = new XSSFWorkbook();
            Sheet firstSheet = libro.createSheet(nombreArhivo);
            setHeader(firstSheet); 
            fillRows(firstSheet);
            

            libro.write(bos);
            libro.close(); 
            archivo = bos.toByteArray();
            
            bos.flush();
            bos.close();

        }
        catch(IOException ex){
             Logger.getLogger(ExportadorExcel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        return archivo;
    }    
    
    
    public byte[] fillFirtSheet() {
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XSSFWorkbook libro = null;
        byte[] archivo = null;
        try {            
            
            libro = new XSSFWorkbook();
            Sheet firstSheet = libro.createSheet(nombreArhivo);

            Row rowHeader = firstSheet.createRow((short) 0);

            String[] nombreColumnas = columnaDatos.split(",");
            int contadorCeldas = 0;
            for (int j = 0; j < nombreColumnas.length; j++, contadorCeldas++) {
                String nombreColumna = nombreColumnas[j];
                Cell cell = rowHeader.createCell(contadorCeldas);
                cell.setCellValue(nombreColumnas[j]);
            }

            Iterator<Rows> iteratorRow = arrayRows.iterator();
            int SECOND_ROW_SHEET = 1;
            int rowAcount = SECOND_ROW_SHEET;

            while (iteratorRow.hasNext()) {
                Rows rowdata = iteratorRow.next();
                
                Iterator<Data> iteratorData = rowdata.getDataList().iterator();

                Row row = firstSheet.createRow(rowAcount);
                int cellAcount = 0;
                while (iteratorData.hasNext()) {
                    Data data = iteratorData.next();
                    Cell cell = row.createCell(cellAcount);
                    cell.setCellValue(data.getValue());
                    cellAcount++;
                }
                rowAcount++;
            }

            if (metadataFirtSheet != null) {
                int lastrow = firstSheet.getLastRowNum();
                Row newRow = firstSheet.createRow(lastrow + 2);
                newRow.createCell(0).setCellValue(metadataFirtSheet);
                firstSheet.autoSizeColumn(0);
            }

            libro.write(bos);
            archivo = bos.toByteArray();

        } catch (Exception e) {
            System.out.println("Error exportando archivos:" + e.getMessage());
        } finally {
            
            try {
                libro.close();                
                bos.flush();
                bos.close();
            } catch (IOException ex) {
                Logger.getLogger(ExportadorExcel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
          
        }

        return archivo;
    }
    public boolean writeExcelFile() {
        boolean result = false;
        byte[] data = fillFirstSheet();

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(this.pathArchivo);
            fos.write(data);
            result = true;

        } catch (IOException ex) {
            Logger.getLogger(ExportXLS.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {

            try {
                fos.flush();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ExportXLS.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }

        }
        return result;

    }
    public boolean writeExcelFile(byte[] data) {
        boolean result = false;      

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(this.pathArchivo);
            fos.write(data);
            result = true;
            
            fos.flush();
            fos.close();

        } catch (IOException ex) {
            Logger.getLogger(ExportXLS.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        return result;

    }
    /**
     * Generate excel file data array don�t have comma character.
     *
     */
    public byte[] generaArchivo() {

        String path = null;
        HSSFRow row = null;
        byte[] archivo = null;
        FileOutputStream fos = null;

        HSSFCell celda = null;
        HSSFRichTextString texto = null;
        HSSFWorkbook libro = null;
        try {

            libro = new HSSFWorkbook();
            firstSheet = libro.createSheet(nombreArhivo);
            HSSFRow fila = firstSheet.createRow(0);

            String[] nombreColumnas = columnaDatos.split(",");
            int contadorCeldas = 0;
            for (int j = 0; j < nombreColumnas.length; j++, contadorCeldas++) {
                String nombreColumna = nombreColumnas[j];
                celda = fila.createCell(contadorCeldas);
                texto = new HSSFRichTextString(nombreColumna);
                celda.setCellValue(texto);
            }

            Iterator it = arrayDatos.iterator();
            int contadorFilas = 1;
            while (it.hasNext()) {
                String filaDatos = (String) it.next();
                String[] columnas = filaDatos.split(",");
                fila = firstSheet.createRow(contadorFilas);
                contadorFilas++;
                contadorCeldas = 0;
                for (int i = 0; i < columnas.length; i++, contadorCeldas++) {
                    String datoCelda = columnas[i];
                    celda = fila.createCell(contadorCeldas);
                    texto = new HSSFRichTextString(datoCelda);
                    celda.setCellValue(texto);
                }
            }
            if (metadataFirtSheet != null) {
                int lastrow = firstSheet.getLastRowNum();
                HSSFRow newRow = firstSheet.createRow(lastrow + 2);
                newRow.createCell(0).setCellValue(metadataFirtSheet);
                firstSheet.autoSizeColumn(0);
            }

            archivo = libro.getBytes();
            filebyte = libro.getBytes();
            workbook = libro;

        } catch (Exception e) {
            System.out.println("Error exportando archivos:" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return archivo;
    } 
    
    public byte[] generaArchivoWithPOI() {

        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Data");
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        String[] columnName = columnaDatos.split(",");

        data.put("1", columnName);
        Iterator it = arrayDatos.iterator();
        int contador = 2;
        while (it.hasNext()) {
            String dataRow = (String) it.next();
            data.put(String.valueOf(contador), dataRow.split(","));
            contador++;
        }

        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }

        ByteArrayOutputStream bos = null;
        try {
            bos = new ByteArrayOutputStream();
            workbook.write(bos);
            filebyte = bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return filebyte;

    }
    public HSSFWorkbook getWorkbook() {
        return workbook;
    }
    public void setWorkbook(HSSFWorkbook workbook) {
        this.workbook = workbook;
    }
    public byte[] getFilebyte() {
        return filebyte;
    }
    public void setFilebyte(byte[] filebyte) {
        this.filebyte = filebyte;
    }
    public String getMetadataFirtSheet() {
        return metadataFirtSheet;
    }
    public void setMetadataFirtSheet(String metadataFirtSheet) {
        this.metadataFirtSheet = metadataFirtSheet;
    }

    public String getColumnaDatos() {
        return columnaDatos;
    }

    public void setColumnaDatos(String columnaDatos) {
        this.columnaDatos = columnaDatos;
    }

    public String getDataSeparator() {
        return dataSeparator;
    }

    public void setDataSeparator(String dataSeparator) {
        this.dataSeparator = dataSeparator;
    }
}

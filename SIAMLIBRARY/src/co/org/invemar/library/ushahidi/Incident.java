package co.org.invemar.library.ushahidi;

import java.util.ArrayList;

public class Incident {
    private String domain;   
     private String id;
     private String title;
     private String descripcion;
     private String date;
     private String active;
     private String locationName;
     private String latitute;
     private String longitude;
     
     private ArrayList<Category> category;
     private ArrayList<CustomFields> customsFieldList;
     

       public String getId() {
           return id;
       }

       public void setId(String id) {
           this.id = id;
       }

       public String getTitle() {
           return title;
       }

       public void setTitle(String title) {
           this.title = title;
       }

       public String getDescripcion() {
           return descripcion;
       }

       public void setDescripcion(String descripcion) {
           this.descripcion = descripcion;
       }

       public String getDate() {
           return date;
       }

       public void setDate(String date) {
           this.date = date;
       }

       public String getActive() {
           return active;
       }

       public void setActive(String active) {
           this.active = active;
       }

       public String getLocationName() {
           return locationName;
       }

       public void setLocationName(String locationName) {
           this.locationName = locationName;
       }

       public String getLatitute() {
           return latitute;
       }

       public void setLatitute(String latitute) {
           this.latitute = latitute;
       }

       public String getLongitude() {
           return longitude;
       }

       public void setLongitude(String longitude) {
           this.longitude = longitude;
       }

       public ArrayList<Category> getCategory() {
           return category;
       }

       public void setCategory(ArrayList<Category> category) {
           this.category = category;
       }

       public String getDomain() {
           return domain;
       }

       public void setDomain(String domain) {
           this.domain = domain;
       }

       public ArrayList<CustomFields> getCustomsFieldList() {
           return customsFieldList;
       }

       public void setCustomsFieldList(ArrayList<CustomFields> customsFieldList) {
           this.customsFieldList = customsFieldList;
       }
     
}

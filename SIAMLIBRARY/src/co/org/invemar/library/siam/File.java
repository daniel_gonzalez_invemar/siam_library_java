/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam;

import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.xmlbeans.impl.jam.internal.DirectoryScanner;

/**
 *
 * @author usrsig15
 */
public class File {

    public void deleteFileDirectoryByCommandSO(String command) {

        try {
            
            Runtime.getRuntime().exec(command);
        } catch (IOException ex) {
            Logger.getLogger(File.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    public String getExtension(String filename) {
        String exten = "";
        try {
            int index = filename.lastIndexOf('.');

            if (index != -1) {
                exten = filename.substring(index + 1);
            }

        } catch (Exception ex) {
            Logger.getLogger(File.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exten;
    }

    public boolean deleteFile(String filename) {
        boolean result = false;
        java.io.File file = new java.io.File(filename);
        //System.out.println("ruta de archivo a borrar:"+filename);
        if (file.exists()) {
            file.delete();
            System.out.println("file exits");
            result = true;
        }
        return result;
    }

    public boolean copyFile(String ruta, String fileName, InputStream in, int tamanio) {
        boolean resultado = false;
        try {

            deleteFile(fileName);
            OutputStream out = new FileOutputStream(new java.io.File(ruta + java.io.File.separator + fileName));

            int read = 0;
            byte[] bytes = new byte[tamanio];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
            resultado = true;
        } catch (IOException e) {
            System.out.println("Error copiando el archivo:" + e.getMessage());
        }
        return resultado;
    }

    public static void main(String[] args) {
        co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();
        FinderFile ff = new FinderFile();
        f.deleteFileDirectoryByCommandSO("rmdir /s /q 'C:\\Users\\usrsig15\\Documents\\NetBeansProjects\\argosjava\\argos\\build\\web\\downloaddata'");
        

    }

}

/*
 * ConnectionFactory.java
 *
 * Created on 5 de junio de 2007, 03:41 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package co.org.invemar.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * 
 * @author Julian Jose Pizarro Pertuz
 */
public class ConnectionFactory {

    static InitialContext initialContext;
    private String defaultConnectionResorce = "usuario";

    static {
        try {
            initialContext = new InitialContext();

        } catch (NamingException ne) {

            System.out.println("Error!! No se ha podido crear el Objeto InitialContext " + ne);
        }
    }

    private synchronized DataSource getDataSource(String connectionResource) {
        DataSource data;
        Context envContext;
        try {
            envContext = (Context) initialContext.lookup("java:comp/env");
            data = (DataSource) envContext.lookup("jdbc/" + connectionResource);

            //System.out.println("DataSource obtenido exitosamente");
            return data;
        } catch (Exception ex) {
            System.out.println("ERROR en getDataSource:" + ex);
            return null;
        }
    }
    

    public synchronized Connection createConnection() throws SQLException {
        Connection con = getDataSource(defaultConnectionResorce).getConnection();
        return con;
    }

    public synchronized Connection createConnection(String connectionResource) throws SQLException {
        Connection con = getDataSource(connectionResource).getConnection();
        return con;
    }
    public synchronized Connection createConnection(String user, String password, String IP,String scheme )  throws SQLException{
         Connection con =null;
		 try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            String url = "jdbc:oracle:thin:@"+IP+":1521:"+scheme+"";	          
	            con = DriverManager.getConnection(url, user, password);
	            

	        } catch (ClassNotFoundException e) {
	            System.out.println("Error connection:" + e.getMessage());

	        } catch (Exception e) {
	            System.out.println("Error connection:" + e.getMessage());
	        }
	        return con;
    }

    public void closeConnectionInstancia(Connection connection) {

        if (connection != null) {
            try {
                connection.close();

            } catch (SQLException e) {
                System.out.println("Error closeConnection " + e);
            } finally {
                connection = null;
            }
        }

    }
     public static void closeConnection(Connection connection) {

        if (connection != null) {
            try {
                connection.close();

            } catch (SQLException e) {
                System.out.println("Error closeConnection " + e);
            } finally {
                connection = null;
            }
        }

    }

  
}

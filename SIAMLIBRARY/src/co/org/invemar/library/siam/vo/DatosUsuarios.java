/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam.vo;

/**
 *
 * @author usrsig15
 */
public class DatosUsuarios {
   private String idProyecto;
   private String idUsuario;
   private String vinculado ;
   private String nombre;
   private String apellido;
   
   
   
    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getVinculado() {
        return vinculado;
    }

    public void setVinculado(String vinculado) {
        this.vinculado = vinculado;
    }
   

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
   
}

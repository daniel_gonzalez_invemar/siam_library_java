/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam.exportadores;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author usrsig15
 */
public class ExportXLS {

    private ArrayList<String> nameColumns;
    private ArrayList<Rows> datalist;
    private String fileName;

    public static void main(String[] args) {

        ArrayList<String> columnName = new ArrayList<String>();
        columnName.add("c1");
        columnName.add("c2");
        columnName.add("c3");
        columnName.add("c4");

        ArrayList<Rows> rowsData = new ArrayList<Rows>();
        Rows r = new Rows();
        r.addData(new Data("1"));
        r.addData(new Data("2"));
        r.addData(new Data("3"));
        r.addData(new Data("4"));
        rowsData.add(r);

        r = new Rows();
        r.addData(new Data("5"));
        r.addData(new Data("6"));
        r.addData(new Data("0"));
        r.addData(new Data("0"));
        rowsData.add(r);

        String fileName = "C:\\documentos\\data" + Calendar.getInstance().getTimeInMillis() + ".xls";
        ExportXLS exportxls = new ExportXLS(fileName, columnName, rowsData);
        exportxls.generateExcelFile();

    }

    public ExportXLS(String filename, ArrayList<String> namesColumns, ArrayList<Rows> datalist) {
        this.nameColumns = namesColumns;
        this.datalist = datalist;
        this.fileName = filename;

    }

    public boolean writeExcelFile() {
        boolean result = false;
        byte[] data = generateExcelFile();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fileName);
            fos.write(data);
            result = true;

        } catch (IOException ex) {
            Logger.getLogger(ExportXLS.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {

            try {
                fos.flush();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ExportXLS.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }

        }
        return result;

    }

    public byte[] generateExcelFile() {
        byte[] data = null;
        try {

            File file = new File(this.fileName);
            file.createNewFile();

            FileOutputStream fileOut = new FileOutputStream(this.fileName);
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet worksheet = workbook.createSheet("POI Worksheet");

            HSSFRow row1 = worksheet.createRow((short) 0);
            for (int i = 0; i < nameColumns.size(); i++) {

                HSSFCell cell = row1.createCell(i);
                cell.setCellValue(nameColumns.get(i));
                HSSFCellStyle cellStyle = workbook.createCellStyle();
                cell.setCellStyle(cellStyle);
            }

            for (int r = 0; r < datalist.size(); r++) {
                row1 = worksheet.createRow((short) r + 1);
                Rows row = datalist.get(r);

                for (int i = 0; i < nameColumns.size(); i++) {
                    HSSFCell cell = row1.createCell(i);
                    cell.setCellValue(row.getData(i).getValue());
                    HSSFCellStyle cellStyle = workbook.createCellStyle();
                    cell.setCellStyle(cellStyle);
                }
            }

            workbook.write(fileOut);

            Path path = Paths.get(this.fileName);
            data = Files.readAllBytes(path);

            fileOut.flush();
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public String getFileName() {
        return fileName;
    }

}

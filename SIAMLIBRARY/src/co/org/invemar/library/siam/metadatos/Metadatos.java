/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam.metadatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import co.org.invemar.library.siam.*;
import co.org.invemar.library.siam.vo.Responsable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class Metadatos {

    public Map<String, String> getImpactosConocidos() {
        String query = "SELECT * FROM METADATOS where categoria_id=1 and codigoatributo=1 order by valor";
        return getListado(query, "getImpactosConocidos");
    }

    public Map<String, String> getTodosIntervencionesAntropicas() {
        String query = "SELECT * FROM METADATOS where categoria_id=1 and codigoatributo=3 order by valor";
        return getListado(query, "getTodosIntervencionesAntropicas");
    }

    public Map<String, String> getTodosActividadesEconomicas() {
        String query = "SELECT * FROM METADATOS where categoria_id=1 and codigoatributo=2 order by valor";
        return getListado(query, "getTodosActividadesEconomicas");
    }

    private Map<String, String> getListado(String query, String nombreMetodo) {
        Map<String, String> listado = new HashMap<String, String>();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        ConnectionFactory conectionfactory = new ConnectionFactory();

        try {
            con = conectionfactory.createConnection("metadatos");
            pstmt = con.prepareStatement(query);
            //System.out.println("query:"+query);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                listado.put(rs.getString("VALOR"), rs.getString("IDVALOR"));
            }
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto Metadatos  metodo " + nombreMetodo + ":" + ex.getMessage());

        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return listado;
    }

    @Deprecated
    public ArrayList<Responsable> getResponsableDatos(String cadenasAnioMuestreo, String tematica) {
        Connection con = null;

        ConnectionFactory conectionfactory = new ConnectionFactory();
        ArrayList<Responsable> responsables = new ArrayList<Responsable>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String cadenaAnios = cadenasAnioMuestreo.substring(0, cadenasAnioMuestreo.length() - 1);
        //System.out.println("cadenasAnioMuestreo:" + cadenasAnioMuestreo);
        String query = "select DISTINCT nombre,\n"
                + "  apellido,nvl(INSTITUTO_CO,'-') as entidad\n"
                + "  FROM\n"
                + "  (\n"
                + "    SELECT\n"
                + "      mn.id_muestreo,\n"
                + "      TO_CHAR(r.fecha,'yyyy') AS anio,\n"
                + "      r.uploadflag ,\n"
                + "      dc.nombre_co as nombre, \n"
                + "      dc.APELLIDOS_CO as apellido,\n"
                + "      dc.INSTITUTO_CO\n"
                + "    FROM\n"
                + "      (V_MANGLARES_NACIONALES mn\n"
                + "    INNER JOIN bresponsables r\n"
                + "    ON\n"
                + "      mn.id_muestreo=r.muestreo )\n"
                + "    INNER JOIN cdircolector dc\n"
                + "    ON\n"
                + "      dc.codigo_co=r.codigo_funcionario        \n"
                + "    WHERE\n"
                + "      id_tematica                =" + tematica + "\n"
                + "    AND TO_CHAR(r.fecha,'yyyy') IN (" + cadenaAnios + ")\n"
                + "    ORDER BY\n"
                + "      id_muestreo )";

        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            //pstmt.setString(1, tematica);
            //System.out.println("query tomador de muestra:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Responsable responsable = new Responsable();
                responsable.setNombre(rs.getString("nombre"));
                responsable.setApellido(rs.getString("apellido"));
                responsable.setVinculado(rs.getString("entidad"));
                responsables.add(responsable);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Metadatos.class.getName()).log(Level.SEVERE, null, ex.getMessage());

        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return responsables;
    }

    public ArrayList<Responsable> getResponsableDatosBySector(Connection con, long codSector, String cadenasAnioMuestreo, String tematica) {

        ConexionPrueba cp = new ConexionPrueba("monitoreom", "mon2007");
        con = cp.getConn();
        // ConnectionFactory conectionfactory = new ConnectionFactory();
        ArrayList<Responsable> responsables = new ArrayList<Responsable>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String cadenaAnios = cadenasAnioMuestreo.substring(0, cadenasAnioMuestreo.length() - 1);

        String query = "SELECT "
                + "  nombre_co as nombre, "
                + "  apellidos_co as apellido, "
                + "  INSTITUTO_CO  as entidad "
                + "FROM "
                + "  cdircolector "
                + "WHERE "
                + "  codigo_co IN "
                + "  ( "
                + "    SELECT DISTINCT "
                + "      codigo_co "
                + "    FROM "
                + "      bresponsables br "
                + "    JOIN cdircolector dc "
                + "    ON "
                + "      br.codigo_funcionario=dc.codigo_co "
                + "    WHERE tarea=4 and "
                + "      muestreo IN "
                + "      ( "
                + "        SELECT "
                + "          id_muestreo "
                + "        FROM "
                + "          V_MANGLARES_NACIONALES mn "
                + "        WHERE "
                + "          id_tematica="+tematica+" "
                + "        AND ano      in("+cadenaAnios+") "
                + "        AND codsector="+codSector+"      ) "
                + "  )";

        try {

            pstmt = con.prepareStatement(query);
            //pstmt.setString(1, tematica);
            System.out.println("query tomador de muestra:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Responsable responsable = new Responsable();
                responsable.setNombre(rs.getString("nombre"));
                responsable.setApellido(rs.getString("apellido"));
                responsable.setVinculado(rs.getString("entidad"));
                responsables.add(responsable);
            }

        } catch (Exception ex) {
            Logger.getLogger(Metadatos.class.getName()).log(Level.SEVERE, null, ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(Metadatos.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            }
        }
        return responsables;
    }

}

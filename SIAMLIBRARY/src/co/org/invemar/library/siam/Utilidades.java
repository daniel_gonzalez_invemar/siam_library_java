/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class Utilidades {

    private String defaultValue = "-";

    public String convertToChartSet(String chartSet,String stringToConvert) {
        String converted = "";
        Charset charset = Charset.forName(chartSet);
        CharsetDecoder decoder = charset.newDecoder();
        CharsetEncoder encoder = charset.newEncoder();

        try {
    // Convert a string to ISO-LATIN-1 bytes in a ByteBuffer
            // The new ByteBuffer is ready to be read.
            ByteBuffer bbuf = encoder.encode(CharBuffer.wrap(stringToConvert));

    // Convert ISO-LATIN-1 bytes in a ByteBuffer to a character ByteBuffer and then to a string.
            // The new ByteBuffer is ready to be read.
            CharBuffer cbuf = decoder.decode(bbuf);
            converted = cbuf.toString();
        } catch (CharacterCodingException e) {
            System.out.println("Error in Utilidades.convertToChartSet:"+e.getMessage());
        }
        return converted;

    }

    public String valueByDefault(String variable) {
        String value = defaultValue;
        if (variable != null) {
            value = variable;
        }
        return value;
    }

    public String ChangeValueByString(String source, String value, String replace) {
        String newString = "";
        if (source.equalsIgnoreCase(value)) {
            newString = replace;
        } else {
            newString = source;
        }
        return newString;
    }

    public String FindStringAndReplaceByOtherStringWithSeparator(String separator, String reviewString, String findString, String newString, boolean includeSeparator) {

        int index = reviewString.indexOf(findString);
        String subStrings = reviewString.substring(index);
        int positionComma = subStrings.indexOf(",");
        String valueToReplace = "";

        if (positionComma == -1) {
            valueToReplace = subStrings.substring(0, subStrings.length());
        } else {
            if (includeSeparator) {
                valueToReplace = subStrings.substring(0, positionComma + 1);
            } else {
                valueToReplace = subStrings.substring(0, positionComma);
            }

        }

        String nstring = reviewString.replace(valueToReplace, newString);

        return nstring;
    }

    public double redondear(double numero, int decimales) {
        return Math.round(numero * Math.pow(10, decimales)) / Math.pow(10, decimales);
    }

    public String valueByDefault(String variable, String defaultValue) {
        this.defaultValue = defaultValue;
        return valueByDefault(variable);
    }

    public Date StringToDate(String dato, String formatDate) {
        Date datoDate = null;
        SimpleDateFormat formatter = new SimpleDateFormat(formatDate);
        try {

            datoDate = formatter.parse(dato);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return datoDate;

    }

    public String RecodeString(String stringToFix) {
        String newString = null;
        try {

            newString = new String(stringToFix.getBytes("ISO-8859-1"), "UTF-8");

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newString;
    }

    public String RecodeString_ISO88591(String stringToFix) {
        String newString = null;
        try {

            newString = new String(stringToFix.getBytes("UTF-8"), "ISO-8859-1");

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newString;
    }

}

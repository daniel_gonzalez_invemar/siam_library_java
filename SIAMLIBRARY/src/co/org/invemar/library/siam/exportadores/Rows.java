/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.library.siam.exportadores;

import java.util.ArrayList;

/**
 *
 * @author usrsig15
 */
public class Rows {
    private ArrayList<Data> dataList;
 
    public Rows() {
        dataList = new ArrayList<Data>();
    }
    
    
    
    public void addData(Data data){
        dataList.add(data);
    }
    
    public Data getData(int position){
        return dataList.get(position);
    }

    public ArrayList<Data> getDataList() {
        return dataList;
    }

    public void setDataList(ArrayList<Data> dataList) {
        this.dataList = dataList;
    }

  
   

   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.library.siam;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class FactoryConexion {
    private Connection con;
    public String Error;

    public FactoryConexion(String ip,String scheme, String user, String pwd) {
       
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@"+ip+":1521:"+scheme+"";
            con = DriverManager.getConnection(url, user, pwd);
            
        } catch (ClassNotFoundException e) {
           System.out.println("Error connection:"+e.getMessage());
        } catch (SQLException e) {
            System.out.println("Error connection:"+e.getMessage());
        }
       
        
    }
    public void closeConnection(){
        try {
            con.close();
            con=null;
        } catch (SQLException ex) {
            Logger.getLogger(FactoryConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
}

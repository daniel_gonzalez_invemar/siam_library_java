/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam.exportadores;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

/**
 *
 * @author usrsig15
 */
public class ExportCSV {

    private String fileHeader;
    private String delimiter;
    private String newLineSeparator;

    public ExportCSV(String fileHeader, String commaDelimiter, String newLineSeparator) {
        this.fileHeader = fileHeader;
        this.delimiter = commaDelimiter;
        this.newLineSeparator = newLineSeparator;
    }
    
    public void writeCsvFile(String fileName, ArrayList<Rows> rows) {
        FileWriter fileWriter = null;

        try {

            fileWriter = new FileWriter(fileName);
            fileWriter.append(fileHeader);
            fileWriter.append(delimiter);
            fileWriter.append(newLineSeparator);

            for (int i = 0; i < rows.size(); i++) {
                Rows r = rows.get(i);

                for (int col = 0; col < r.getDataList().size(); col++) {
                    Data d = r.getData(col);
                    fileWriter.append(d.getValue());
                    fileWriter.append(delimiter);

                }
                fileWriter.append(newLineSeparator);

            }           
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!"+e.getMessage());
            e.printStackTrace();

        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }

        }

    }

    public String getFileHeader() {
        return fileHeader;
    }

    public void setFileHeader(String fileHeader) {
        this.fileHeader = fileHeader;
    }

    public String getCommaDelimiter() {
        return delimiter;
    }

    public void setCommaDelimiter(String commaDelimiter) {
        this.delimiter = commaDelimiter;
    }

    public String getNewLineSeparator() {
        return newLineSeparator;
    }

    public void setNewLineSeparator(String newLineSeparator) {
        this.newLineSeparator = newLineSeparator;
    }

}

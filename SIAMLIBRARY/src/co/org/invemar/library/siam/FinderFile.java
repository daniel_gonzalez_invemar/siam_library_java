/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam;

import java.io.File;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class FinderFile {

    private String pathfile = "";
    private String extension = "";
    int contador = 0;

    public boolean findFile(String name, File file) {
        boolean result = false;
        try {
            File[] list = file.listFiles();

            if (list != null) {
                // System.out.println("lista no es vacio");
                for (File fil : list) {
                    String filename = fil.getName().substring(0, fil.getName().length() - 4);
                    if (fil.isDirectory()) {
                        findFile(name, fil);

                    } else if (name.equalsIgnoreCase(filename)) {
                        co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();
                        extension = f.getExtension(fil.getName());
                        result = true;
                        pathfile = fil.getPath();
                       
                       
                    }

                }
            } else {
                System.out.println("empty directory");
            }
        } catch (Exception e) {
            System.out.println("Error buscando el archivo:" + e.getMessage());
        }
        return result;
    }
    
    

    public void findFileWithExtension(String name, File file) {
        File[] list = file.listFiles();
        if (list != null) {
            for (File fil : list) {
                if (fil.isDirectory()) {
                    findFile(name, fil);
                } else if (name.equalsIgnoreCase(fil.getName())) {
                    co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();
                    extension = f.getExtension(fil.getName());
                }
                pathfile = fil.getPath();
            }
        } else {
            System.out.println("empty directory");
        }
    }

    public static void main(String[] args) {
        FinderFile ff = new FinderFile();

        //ff.findFile("819", new File("C:\\D\\Desarrollo Software\\Proyectos de Software\\Argos\\Argos\\build\\web\\ingresos\\manglares\\kml\\"));
        ff.findFile("949", new File("C:\\D\\Desarrollo Software\\Proyectos de Software\\Argos\\Argos\\build\\web\\ingresos\\manglares\\fotos"));

//        System.out.println("nombre y extension:" + ff.getPathfile());
//        System.out.println("Extension:" + ff.getExtension());
    }

    public String getPathfile() {
        return pathfile;
    }

    public String getExtension() {
        return extension;
    }

}

package co.org.invemar.library.ushahidi;

public class CustomFields {
    private String fieldId;
       private String formdId;
       private String fieldName;
       private String FieldType;
       private String fieldDefault;
       private String FieldResponse;
       private String FieldRequired;

        public String getFieldId() {
            return fieldId;
        }

        public void setFieldId(String fieldId) {
            this.fieldId = fieldId;
        }

        public String getFormdId() {
            return formdId;
        }

        public void setFormdId(String formdId) {
            this.formdId = formdId;
        }

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getFieldType() {
            return FieldType;
        }

        public void setFieldType(String FieldType) {
            this.FieldType = FieldType;
        }

        public String getFieldDefault() {
            return fieldDefault;
        }

        public void setFieldDefault(String fieldDefault) {
            this.fieldDefault = fieldDefault;
        }

        public String getFieldResponse() {
            return FieldResponse;
        }

        public void setFieldResponse(String FieldResponse) {
            this.FieldResponse = FieldResponse;
        }

        public String getFieldRequired() {
            return FieldRequired;
        }

        public void setFieldRequired(String FieldRequired) {
            this.FieldRequired = FieldRequired;
        }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.library.siam.convertirs;

import co.org.invemar.library.siam.Utilidades;

/**
 *
 * @author usrsig15
 */
public class Convert {

    public double GrateMinuteAndSecondToDecimal(double grade, int minute, double second) {
        double decimal;          
       
        double m=minute/60d;       
        
        double s=second/3600d;               
        
        if (grade>0) {
            decimal = grade + m+ s;  
        }else{
            decimal = grade - m - s; 
        }
        return decimal;

    }

    public CoordenateGrade DecimalToGrateMinuteAndSecond(double number) {
            
        CoordenateGrade coordenate = new CoordenateGrade();
        int g = (int) number;
        int m = (int) ((number - g) * 60);
        double s = (((number - g) - ((double)m/ 60d)) * 3600d); 
        long second = Math.round(s);       
        
        coordenate.setGrade(g);
        coordenate.setMinute(m);
        coordenate.setSecond(second);       
       
        return coordenate;

    }

    public static void main(String[] args) {
        Convert s = new Convert();
        System.out.println(s.GrateMinuteAndSecondToDecimal(73, 15, 20.5));
        //System.out.println(15/60d);
        //-73.25569444444444
        //73.25569444444444
 //73.255556
        //CoordenateGrade coor = s.DecimalToGrateMinuteAndSecond(10.008333333333333);
      //  System.out.println("grade: " + coor.getGrade());
     //   System.out.println("minute: " + coor.getMinute());
       // System.out.println("second: " + coor.getSecond());

    }
}
